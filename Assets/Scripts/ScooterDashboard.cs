﻿
using UnityEngine;
using UnityEngine.UI;
using static WRLDSUtils;
using static WRLDSScooterPlugin;
using System.Collections;

public class ScooterDashboard : Singleton<WRLDSScooterPlugin>
{
    public Rigidbody scooter;

    private float roll;
    private float pitch;
    private float previousTilt;
    public Text tiltText;
    private bool tiltReceived; 

    private Direction direction;
    public Text directionText; 

    private double latitude;
    private double longitude;
    public Text coordinates;

    private bool deviceConnected;
    public Button firmwareVersionEvent;
    public Button firmwareVersionStream;

    public Button tiltOffset;
    public Button calibrateSensor;

    public Transform progressBar;
    public Text sensorCalibrationPercentage;
    private int calibrationProgress = 0;
    private bool calibrationProgressReceived = false;

    public Text offlineActivityDurationText;
    private int offlineActivityDuration = 0;
    private bool offlineActivityDurationReceived;

    private void Awake()
    {
#if UNITY_ANDROID
        WRLDS.Init();
#endif
    }
    void Start()
    {
#if UNITY_ANDROID

        /**
         * Retrieves Phone's GPS coordinates
         * See -> WRLDSScooterPlugin.GetGPSCoordinates
         */
        WRLDS.GetGPSCoordinates(true, 10, 0, (double latitude, double longitude) =>
        {
            this.latitude = latitude;
            this.longitude = longitude;
        });

        //================================================================================
        // Firmware version < 1.1.3: Acceleration Event methods 
        //================================================================================
        WRLDS.SetHighGThreshold(8);

        /**
         * Triggered when a high G event is received from the firmware
         * See -> AndroidHighGCallback.onHighGReceived
         */
        WRLDS.GetHighGEvents(true, (BluetoothDevice device, Axis axis, float sumG, Intensity accelerationIntensity) => {
            Debug.Log("HighG event:  Axis: " + axis + " SumG: " + sumG + " Intensity: " + accelerationIntensity);
        });

        //================================================================================
        // Firmware version >= 1.1.3: Acceleration Stream methods
        //================================================================================

        /**
         * Retrieves raw acceleration data. If you would like to unsubscribe from listeners you can (optionally) use a separate 
         * handler that can be unsubscribed from in OnDestroy() 
         * 
         * See -> AndroidRawAccelerationDataCallback.onRawAccelerationDataReceived
         */
        WRLDS.StartAccelerationDataStream(AccelerationDataHandler);

        /**
         * Gets the tilt of the sensor relative to the ground         
         * See -> AndroidTiltCallback.onTiltReceived
         */
        WRLDS.TiltEvents(true, (BluetoothDevice device, float roll, float pitch) => {
            this.roll = roll;
            this.pitch = pitch;
            tiltReceived = true;
        });

        /**
         * See -> AndroidDirectionStateCallback.onDirectionStateReceived
         */
        WRLDS.DirectionStateEvents(true, (BluetoothDevice device, Direction direction) =>
        {
            this.direction = direction;
        });

        WRLDS.StartConnectionStateEvents(ConnectionStateHandler);

        WRLDS.SetTiltDirectionThreshold(8);

        WRLDS.ServicesDiscoveredEvents(true, (device, optionalServicesDiscovered) =>
        {
            WRLDS.ReadOfflineActivityDuration(true, (bluetoothDevice, duration) =>
            {
                Debug.Log("Unity onOfflineActivityDurationReceived: " + duration + " seconds");
                offlineActivityDuration = duration;
                offlineActivityDurationReceived = true;
            });
        });

        firmwareVersionEvent.interactable = false;
        firmwareVersionStream.interactable = false;
        firmwareVersionEvent.onClick.AddListener(OnFirmwareVersionEventRequested);
        firmwareVersionStream.onClick.AddListener(OnFirmwareVersionStreamRequested);
        tiltOffset.onClick.AddListener(OnTiltOffsetClicked);
        calibrateSensor.onClick.AddListener(OnCalibrateSensorClicked);

        progressBar.localScale = new Vector3(100f, calibrationProgress);

#endif
    }


    void Update()
    {
#if UNITY_ANDROID
        if (tiltReceived)
        {
            float scooterTransform = smoothingFilter(roll, 0.9f);
            scooter.transform.eulerAngles = new Vector3(0, 0, -scooterTransform);
        }

        directionText.text = direction.ToString();
        tiltText.text = Mathf.Round(roll).ToString();
        coordinates.text = "latitude: " + latitude.ToString() + System.Environment.NewLine + " longitude: " + longitude.ToString();

        if (deviceConnected)
        {
            firmwareVersionEvent.interactable = true;
            firmwareVersionStream.interactable = true;
        }
        else
        {
            firmwareVersionEvent.interactable = false;
            firmwareVersionStream.interactable = false;
        }

        if (calibrationProgressReceived)
        {
            progressBar.localScale = new Vector3(100f, calibrationProgress);
            sensorCalibrationPercentage.text = calibrationProgress.ToString() + " %";
        }

        if (offlineActivityDurationReceived)
        {
            offlineActivityDurationText.text = offlineActivityDuration + " s";
        }
#endif
    }

    private void OnDestroy()
    {
#if UNITY_ANDROID
        WRLDS.StopAccelerationDataStream(AccelerationDataHandler);
        WRLDS.StopConnectionStateEvents(ConnectionStateHandler);
#endif
    }

#if UNITY_ANDROID
    void ConnectionStateHandler(BluetoothDevice device, int connectionState)
    {
        if (connectionState == 0)
            deviceConnected = false;
        else if (connectionState == 1)
            deviceConnected = true;
    }

    void AccelerationDataHandler (BluetoothDevice bluetoothDevice, float[] motionData) 
    {
        //Debug.Log("X-axis: " + motionData[0].ToString());
    }

    void OnFirmwareVersionEventRequested()
    {
        string result = WRLDS.RequestFirmwareVersionUpdate(AccelerationMode.EVENTS);
        Debug.Log("FirmwareUpdateRequest: " + result);
    }

    void OnFirmwareVersionStreamRequested()
    {
        string result = WRLDS.RequestFirmwareVersionUpdate(AccelerationMode.STREAM);
        Debug.Log("FirmwareUpdateRequest: " + result);
    }

    void OnTiltOffsetClicked()
    {
        WRLDS.SetTiltOffset(roll);
    }

    void OnCalibrateSensorClicked()
    {
        WRLDS.CalibrateSensorOrientation(true, (BluetoothDevice device, int progress) => {
            calibrationProgressReceived = true;
            calibrationProgress = progress;
        });
    }

    float smoothingFilter(float tilt, float bias)
    {
        return previousTilt = bias * previousTilt + (1 - bias) * tilt;
    }
#endif
}