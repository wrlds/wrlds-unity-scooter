﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WRLDSUtils;

public class WRLDSScooterPlugin : WRLDSBasePlugin
{

    #region Fields
#if UNITY_ANDROID
    private AndroidLocationCallback onLocationCallback;
    private AndroidHighGCallback onHighGCallback;
    private AndroidTiltCallback onTiltCallback;
    private AndroidDirectionStateCallback onDirectionStateCallback;
    private AndroidSensorCalibrationCallback onSensorCalibrationCallback;
    private AndroidOfflineActivityDurationCallback onOfflineActivityDurationCallback;
    private AndroidServicesDiscoveredCallback onServicesDiscoveredCallback;


    public enum AccelerationMode
    {
        EVENTS,
        STREAM
    }
#endif
    #endregion

    #region Lifecycle
    /**     
     * <summary>
     *  SDK Initialization is done in #Awake()  
     *  The SDK needs to be instantiated with a reference to the activity 
     * </summary>    
     */
    void Awake()
    {

#if UNITY_ANDROID
        DontDestroyOnLoad(this.gameObject);

        SDK = new AndroidJavaObject("com.wrlds.api.ScooterProvider", new object[] { GetAndroidUnityPlayerActivity() });
        bleDeviceProvider = SDK.Get<AndroidJavaObject>("bleDeviceProvider");
        userProvider = SDK.Get<AndroidJavaObject>("userAccountManager");

        Debug.Log($"SDK initialized -> product type: Scooter");
#else
        Destroy(this);
#endif
    }

    void OnApplicationPause(bool pause)
    {
#if UNITY_ANDROID
        if (!pause)
        {
            if (SDK != null)
                SDK.Call("onStart");
        }
        if (pause)
        {
            if (SDK != null)
                SDK.Call("onStop");
        }
#endif
    }

    void OnDestroy()
    {
#if UNITY_ANDROID
        SDK.Call("onDestroy");
#endif
    }
#endregion


#region  Methods
#if UNITY_ANDROID
    /**
     * The threshold that the acceleration needs to exceed in order to trigger a High G event
     * @param highGThreshold: The threshold in acceleration (m/s2). 9.81m/s2 is equal to 1G acceleration 
     */
    public void SetHighGThreshold(float highGThreshold)
    {
        SDK.CallStatic("setHighGThreshold", highGThreshold);
    }
#endif

#if UNITY_ANDROID
    public float GetHighGThreshold()
    {
        return SDK.CallStatic<float>("getHighGThreshold");
    }
#endif

#if UNITY_ANDROID
    /**
     * 
     */
    public void CalibrateSensorOrientation(bool enable, AndroidSensorCalibrationCallback.SensorCalibrationDelegate dataCallback)
    {
        if (onSensorCalibrationCallback == null)
            onSensorCalibrationCallback = new AndroidSensorCalibrationCallback();

        if (enable)
            onSensorCalibrationCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onSensorCalibrationCallback.RemoveSubscriber(dataCallback); }

        SDK.Call("calibrateSensorOrientation");
    }
#endif

#if UNITY_ANDROID
    /**
     * The threshold that the acceleration needs to exceed in order to trigger a FORWARD or BACKWARD Direction Event
     * @param directionThreshold: The threshold in kilometers per hour (km/h)
     */
    public void SetDirectionEventVelocityThreshold(float velocityThreshold)
    {
        SDK.CallStatic("setDirectionEventVelocityThreshold", velocityThreshold);
    }
#endif

#if UNITY_ANDROID
    public float GetDirectionEventVelocityThreshold()
    {
        return SDK.CallStatic<float>("getDirectionEventVelocityThreshold");
    }
#endif

#if UNITY_ANDROID
    /**
     * The tilt threshold determines at what roll angle a Direction.LEFT or Direction.RIGHT 
     * will be triggered. Direction.IDLE is the state when the tilt does not exceed this threshold
     * 
     * The range of the tilt is between -180 to 180, with 0 being the scooter's exact upright position
     * 
     * If you set the treshold to 8 it means that Direction.LEFT is triggered at -8 degrees
     * and Direction.RIGHT at 8 degrees
     * 
     * Default threshold value is: 6
     * 
     * @param threshold: The tilt threshold in degrees (°)
     */
    public void SetTiltDirectionThreshold(float threshold)
    {
        SDK.CallStatic("setTiltDirectionThreshold", threshold);
    }
#endif

#if UNITY_ANDROID
    public float GetTiltDirectionThreshold()
    {
        return SDK.CallStatic<float>("getTiltDirectionThreshold");
    }
#endif

#if UNITY_ANDROID
    /**
     * The tilt offset indicates the offset value of the accelerometer sensor in it's idle position.
     * In case the sensor is not completely parallel to the ground, 
     * this value can offset the tilt calculations to accurately represent this sensor's tilt
     * 
     * @param offset: The tilt offset in degrees (°)
     */
    public void SetTiltOffset(float offset)
    {
        SDK.CallStatic("setTiltOffset", offset);
    }
#endif

#if UNITY_ANDROID
    public float GetTiltOffset()
    {
        return SDK.CallStatic<float>("getTiltOffset");
    }
#endif

#if UNITY_ANDROID
    public void SetTiltAxis(Axis axis)
    {
        SDK.CallStatic("setTiltAxis", (int) axis );
    }
#endif

#if UNITY_ANDROID
    public Axis GetTiltAxis()
    {
        return (Axis) SDK.CallStatic<int>("getTiltAxis");
    }
#endif


#if UNITY_ANDROID
    /**
     * This timeout specifies the wait period until all direction states are available again
     * 
     * Usecase: When the scooter is tilting/accelerating it will generate direction state events
     * To avoid multiple triggers of this event we set this timeout after which we can generate the event again.
     * 
     * @param milliseconds: The timeout in milliseconds
     */
    public void SetDirectionEventTimeout(int milliseconds)
    {
        SDK.CallStatic("setDirectionEventTimeout", milliseconds);
    }

    /**
     * This timeout specifies the wait period until the specified direction state is triggered again
     * 
     * Usecase: When the scooter is tilting/accelerating it will generate direction state events
     * To avoid multiple triggers of this event we set this timeout after which we can generate the event again.
     * 
     * @param direction: The direction for which you want to set the timeout
     * @param milliseconds: The timeout in milliseconds
     */
    public void SetDirectionEventTimeout(Direction direction, int milliseconds) 
    {
        SDK.CallStatic("setDirectionEventTimeout", (int) direction, milliseconds); 
    }
#endif

#if UNITY_ANDROID
    public float GetDirectionTimeout(Direction direction)
    {
        return SDK.CallStatic<int>("getDirectionTimeout" , (int)direction);
    }
#endif

    /**
     * A function that updates the firmware to 
     * - Event-based Acceleration data on High G (1.1.2)
     * - Streaming Acceleration data (1.1.3)      
     * 
     * The request will create an Android dialog that prompts the user to update to the specified firmware
     */
#if UNITY_ANDROID
    public string RequestFirmwareVersionUpdate(AccelerationMode mode)
    {
        if (mode == AccelerationMode.EVENTS)
            return SDK.Call<string>("requestFirmwareVersion", "1.1.2");

        else if (mode == AccelerationMode.STREAM)
            return SDK.Call<string>("requestFirmwareVersion", "1.1.3");

        else
            return "Invalid request";
    }
#endif


#if UNITY_ANDROID
    public string GetCurrentFirmwareVersion()
    {
        return SDK.Call<string>("getCurrentFirmwareVersion");
    }
#endif

#if UNITY_ANDROID
    /**
     * Read the offline activity duration from the firmware
     * 
     * @param resetOfflineActivityDuration: You can decide whether to reset the offline activity duration in the firmware or to aggregate it over time
     *                                      Currently the internal maximum count inside the firmware is 256 hours. 
     */
    public void ReadOfflineActivityDuration(bool resetOfflineActivityDuration, AndroidOfflineActivityDurationCallback.OfflineActivityDurationDelegate dataCallback)
    {
        if (onOfflineActivityDurationCallback == null)
            onOfflineActivityDurationCallback = new AndroidOfflineActivityDurationCallback();

        onOfflineActivityDurationCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub 

        SDK.Call("readOfflineActivityDuration", resetOfflineActivityDuration, onOfflineActivityDurationCallback);
    }
#endif

#if UNITY_ANDROID
    /**
     * Gets the GPS coordinates of the phone if permissions have been granted.
     * In case they are not granted a dialog shows that asks the user to grant these permissions
     *
     * See Location Manager provided by Android:
     * https://developer.android.com/reference/android/location/LocationManager.html#requestLocationUpdates%28java.lang.String,%20long,%20float,%20android.location.LocationListener%29
     *
     * @param minimumTime: minimum time interval between location updates (seconds)
     * @param minimumDistance: minimum distance between location updates (meters)
     * 
     * @implNote: Currently the GPS coordinates are fetched from the phone. 
     * You will receive GPS updates at a faster rate than the minimumTime if there are oter apps using GPS data that
     * will are shared with the app. 
     * 
     * If either parameter is set to 0 it will not be utilized
     */
    public void GetGPSCoordinates(bool enable, long minimumTime, long minimumDistance, AndroidLocationCallback.LocationDelegate dataCallback)
    {
        SDK.Call("getGPSCoordinates", minimumTime, minimumDistance);

        if (onLocationCallback == null)
            onLocationCallback = new AndroidLocationCallback();

        if (enable)
            onLocationCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onLocationCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public void GetHighGEvents(bool enable, AndroidHighGCallback.HighGDelegate dataCallback)
    {
        if (onHighGCallback == null)
            onHighGCallback = new AndroidHighGCallback();

        if (enable)
            onHighGCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onHighGCallback.RemoveSubscriber(dataCallback); }
    }

#endif
#if UNITY_ANDROID
    public void TiltEvents(bool enable, AndroidTiltCallback.TiltDelegate dataCallback)
    {
        if (onTiltCallback == null)
            onTiltCallback = new AndroidTiltCallback();

        if (enable)
            onTiltCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onTiltCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public void DirectionStateEvents(bool enable, AndroidDirectionStateCallback.DirectionStateDelegate dataCallback)
    {
        if (onDirectionStateCallback == null)
            onDirectionStateCallback = new AndroidDirectionStateCallback();

        if (enable)
            onDirectionStateCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onDirectionStateCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public void ServicesDiscoveredEvents(bool enable, AndroidServicesDiscoveredCallback.ServiceDiscoveredDelegate dataCallback)
    {
        if (onServicesDiscoveredCallback == null)
            onServicesDiscoveredCallback = new AndroidServicesDiscoveredCallback();

        if (enable)
            onServicesDiscoveredCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onServicesDiscoveredCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public class AndroidServicesDiscoveredCallback : AndroidJavaProxy
    {
        public delegate void ServiceDiscoveredDelegate(BluetoothDevice device, bool optionalServicesDiscovered);
        public event ServiceDiscoveredDelegate OnAndroidServicesDiscovered;

        public void AddSubscriber(ServiceDiscoveredDelegate callback) { OnAndroidServicesDiscovered += callback; }
        public void RemoveSubscriber(ServiceDiscoveredDelegate callback) { OnAndroidServicesDiscovered -= callback; }

        public AndroidServicesDiscoveredCallback() : base("com.wrlds.api.listeners.ServicesDiscoveredListener")
        {
            WRLDS.SDK.Call("setServicesDiscoveredListener", this);
        }

        void onServicesDiscovered(AndroidJavaObject device, bool optionalServicesFound)
        {
            OnAndroidServicesDiscovered?.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), optionalServicesFound);
        }
    }
#endif

#if UNITY_ANDROID
    public class AndroidLocationCallback : AndroidJavaProxy
    {
        public delegate void LocationDelegate(double latitude, double longitude);
        public event LocationDelegate OnLocationReceived;

        public void AddSubscriber(LocationDelegate callback) { OnLocationReceived += callback; }
        public void RemoveSubscriber(LocationDelegate callback) { OnLocationReceived -= callback; }

        public AndroidLocationCallback() : base("com.wrlds.api.listeners.scooter.LocationListener")
        {
            WRLDS.SDK.Call("setLocationListener", this);
        }

        void onLocationReceived(double latitude, double longitude)
        {
            if (OnLocationReceived != null)
            { OnLocationReceived.Invoke(latitude, longitude); }
        }
    }
#endif
#endregion


#region  Listeners
#if UNITY_ANDROID
    public class AndroidHighGCallback : AndroidJavaProxy
    {
        public delegate void HighGDelegate(BluetoothDevice device, Axis axis, float sumG, Intensity accelerationIntensity);
        public event HighGDelegate OnHighGReceived;

        public void AddSubscriber(HighGDelegate callback) { OnHighGReceived += callback; }
        public void RemoveSubscriber(HighGDelegate callback) { OnHighGReceived -= callback; }

        public AndroidHighGCallback() : base("com.wrlds.api.listeners.scooter.HighGListener")
        {
            WRLDS.SDK.Call("setHighGListener", this);
        }

        /**
         * Detects a high G event. A high G event in our context is defined as a rapid acceleration of the scooter
         *
         * @param axis: The axis in which the high-G event occurred.
         * @param sumG: The sum of the acceleration of the high G event (m/s2)
         * @param intensity: The intensity of the high G event: SOFT, MEDIUM, HARD
         */
        void onHighGReceived(AndroidJavaObject device, int axis, float sumG, int accelerationIntensity)
        {
            if (OnHighGReceived != null)            
                OnHighGReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), (Axis) axis, sumG, (Intensity) accelerationIntensity);

            device.Dispose();            
        }
    }
#endif

#if UNITY_ANDROID
    public class AndroidSensorCalibrationCallback : AndroidJavaProxy
    {
        public delegate void SensorCalibrationDelegate(BluetoothDevice device, int progress);
        public event SensorCalibrationDelegate OnSensorCalibrationProgressReceived;

        public void AddSubscriber(SensorCalibrationDelegate callback) { OnSensorCalibrationProgressReceived += callback; }
        public void RemoveSubscriber(SensorCalibrationDelegate callback) { OnSensorCalibrationProgressReceived -= callback; }

        public AndroidSensorCalibrationCallback() : base("com.wrlds.api.listeners.scooter.SensorCalibrationListener")
        {
            WRLDS.SDK.Call("setSensorCalibrationListener", this);
        }

        /**
         * Listener for getting the offset of the tilt relative to the ground.
         *
         * For example: The sensor could be misaligned during manufacturing which results in a tilt that has an offset
         * The user will be asked to hold the product still to measure the offset of the sensor.
         * This is done by subtracting linear acceleration from the raw accelerometer values which yields the gravity vector.
         * Only by holding the product still can we get an accurate estimate of the offset
         *
         * We assume that the IDLE position of the scooter is when the handlebar is parallel to the ground
         *
         * @param calibrationProgress: The progress between 0 - 100% of the calibration of the sensor
         */
        void onSensorCalibrationReceived(AndroidJavaObject device, int progress)
        {
            if (OnSensorCalibrationProgressReceived != null)
                OnSensorCalibrationProgressReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), progress);

            device.Dispose();
        }
    }
#endif

#if UNITY_ANDROID
    public class AndroidTiltCallback : AndroidJavaProxy
    {
        public delegate void TiltDelegate(BluetoothDevice device, float roll, float pitch);
        public event TiltDelegate OnTiltReceived;

        public void AddSubscriber(TiltDelegate callback) { OnTiltReceived += callback; }
        public void RemoveSubscriber(TiltDelegate callback) { OnTiltReceived -= callback; }

        public AndroidTiltCallback() : base("com.wrlds.api.listeners.scooter.TiltListener")
        {
            WRLDS.SDK.Call("setTiltListener", this);
        }

        /**
         * Detects a tilt event.
         * @param roll: The roll angle that the scooter is in (degrees)
         */
        void onTiltReceived(AndroidJavaObject device, float roll, float pitch)
        {
            if (OnTiltReceived != null)            
                OnTiltReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), roll, pitch);
            
            device.Dispose();
        }
    }
#endif

#if UNITY_ANDROID
    public class AndroidDirectionStateCallback : AndroidJavaProxy
    {
        public delegate void DirectionStateDelegate(BluetoothDevice device, Direction direction);
        public event DirectionStateDelegate OnDirectionStateReceived;

        public void AddSubscriber(DirectionStateDelegate callback) { OnDirectionStateReceived += callback; }
        public void RemoveSubscriber(DirectionStateDelegate callback) { OnDirectionStateReceived -= callback; }

        public AndroidDirectionStateCallback() : base("com.wrlds.api.listeners.scooter.DirectionStateListener")
        {
            WRLDS.SDK.Call("setDirectionStateListener", this);
        }

        /**
         * Gets the direction that the scooter is going in.
         * Direction.FORWARD & Direction.BACKWARD are generated by driving forward and backward.
         * Direction.LEFT & Direction.RIGHT are generated by tilting left and right
         * 
         * @implNote: The LEFT and RIGHT states are optional since you can analyze the tilt received 
         * from AndroidTiltCallback.onTiltReceived
         */
        void onDirectionStateReceived(AndroidJavaObject device, int direction)
        {
            if (OnDirectionStateReceived != null)
                OnDirectionStateReceived.Invoke(GetJavaBluetoothClass(device), (Direction) direction);

            device.Dispose();
        }
    }
#endif

#if UNITY_ANDROID
    public class AndroidOfflineActivityDurationCallback : AndroidJavaProxy
    {
        public delegate void OfflineActivityDurationDelegate(BluetoothDevice device, int duration);
        public event OfflineActivityDurationDelegate OnOfflineActivityDurationReceived;

        public void AddSubscriber(OfflineActivityDurationDelegate callback) { OnOfflineActivityDurationReceived += callback; }
        public void RemoveSubscriber(OfflineActivityDurationDelegate callback) { OnOfflineActivityDurationReceived -= callback; }

        public AndroidOfflineActivityDurationCallback() : base("com.wrlds.api.listeners.scooter.OfflineActivityDurationListener")
        {
            WRLDS.SDK.Call("setOfflineActivityDurationListener", this);
        }

        /**
         * Listener for getting the duration of activity of the user when the device was not connected to a phone
         * The purpose of this data is to track the activity when the phone is not connected or out of range
         *
         * Definition:
         * - Offline:   When the Wrlds device is not connected to a phone/tablet. 
         * - Activity:  The start and end of the moment a Wrlds device detects significant movement and wakes up
         *              The firmware will stop counting after 2 minutes of inactivity. At this point it will stop sending advertising packets to connect
         * - Duration:  The time in seconds of activity.
         *
         * @param device: The device for which the offline activity duration was gathered
         * @param offlineActivityDuration: The duration of offline activity that the user had in seconds
         *                               Will return -1 if the data cannot be retrieved from the firmware.
         */
        void onOfflineActivityDurationReceived(AndroidJavaObject device, int offlineActivityDuration)
        {
            if (OnOfflineActivityDurationReceived != null)
                OnOfflineActivityDurationReceived?.Invoke(GetJavaBluetoothClass(device), offlineActivityDuration);

            device.Dispose();
        }
    }
#endif

    #endregion

}
